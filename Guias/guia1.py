import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

# clase de menu inicial
class MenuInicial():

    def __init__(self):
        print("Constructor!")
        self.builder = Gtk.Builder()
        self.builder.add_from_file("guia1.glade")
        window = self.builder.get_object("ventanaPrincipal")
        window.connect("destroy", Gtk.main_quit)
        window.set_default_size(800, 600)
        window.set_title("Suman+2")

        self.BotonAceptar = self.builder.get_object("botonAceptarMI")
        self.BotonAceptar.connect("clicked", self.Resultado_ventana)

        self.entrada1 = self.builder.get_object("entry1")
        self.entrada2 = self.builder.get_object("entry2")

        self.BotonLimpiar = self.builder.get_object("botonRehacerMI")
        self.BotonLimpiar.connect("clicked", self.dialogoRehacer)

        self.botonvalor = self.builder.get_object("spinbutton")

        for event in ['activate', 'changed']:
            self.entrada1.connect(event, self.suma)
            self.entrada2.connect(event, self.suma)

        window.show_all()

        self.texto1 = self.entrada1.get_text()
        self.texto2 = self.entrada2.get_text()

    def suma(self, btn=None):

    	self.texto1 = self.entrada1.get_text()
    	self.texto2 = self.entrada2.get_text()

    	numero1 = len(self.texto1)
    	numero2 = len(self.texto2)

    	self.sumafinal = numero1 + numero2
    	self.botonvalor.set_text(str(self.sumafinal))

    def Resultado_ventana(self, btn=None):
    	
    	aux = ventanaResultado(self.texto1, self.texto2, self.sumafinal, self.entrada1, self.entrada2)

    def dialogoRehacer(self, btn=None):

    	aux = dialogoReha(self.entrada1, self.entrada2)
    	print("COMENZANDO A BORRAR")

# clase que configura la ventana de resultados
class ventanaResultado():

	def __init__(self, texto1, texto2, sumafinal, entrada1, entrada2):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("guia1.glade")
		self.resultado = self.builder.get_object("ventanaResultados")
		self.resultado.set_default_size(700, 300)
		self.resultado.set_title("Resultados")

		self.texto1 = texto1
		self.texto2 = texto2
		self.entrada1 = entrada1
		self.entrada2 = entrada2

		medicion_largo_total = len(self.texto1) + len(self.texto2)

		self.acepto = self.builder.get_object("AceptarBoton")
		self.acepto.connect("clicked", self.limpiartodo)

		self.Label_A_Modificar1 = self.builder.get_object("labelResultadoTxt1")
		self.Label_A_Modificar2 = self.builder.get_object("labelResultadoTxt2")
		self.Label_Suma = self.builder.get_object("labelResultadoCaracteres")

		self.Label_Suma.set_text(str(medicion_largo_total))
		self.Label_A_Modificar1.set_text(self.texto1)
		self.Label_A_Modificar2.set_text(self.texto2)

		self.resultado.show_all()

	def limpiartodo(self, btn=None):
		self.entrada1.set_text("")
		self.entrada2.set_text("")
		self.resultado.destroy()

# clase para abrir el dialogo de rehacer
class dialogoReha():

	def __init__(self, entrada1, entrada2):

		self.entrada1 = entrada1
		self.entrada2 = entrada2
		
		self.builder = Gtk.Builder()
		self.builder.add_from_file("guia1.glade")
		self.dialogoo = self.builder.get_object("dialogoReiniciarAceptar")
		self.dialogoo.set_default_size(700, 300)
		self.dialogoo.set_title("Confirmar")

		self.BotonConfirmar2 = self.builder.get_object("botonACEPTARR")
		self.BotonCancelar2 = self.builder.get_object("botonCANCELARR")

		self.dialogoo.show_all()

		self.BotonConfirmar2.connect("clicked", self.mostrarConfirmacion)
		self.BotonCancelar2.connect("clicked", self.cerrar)

	def mostrarConfirmacion(self, btn=None):

		aux = notificacion()
		print("CONFIRMANDO...")
		self.entrada1.set_text("")
		self.entrada2.set_text("")
		self.dialogoo.destroy()

	def cerrar(self, btn=None):

		print("CERRANDO...")
		self.dialogoo.destroy()

class notificacion():

	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("guia1.glade")
		self.notification = self.builder.get_object("notificacionFinal")
		self.notification.set_default_size(700, 300)
		self.notification.set_title("Cerrando")

		self.Aceptar = self.builder.get_object("botonFinalAc")

		self.notification.show_all()

		self.Aceptar.connect("clicked", self.acept)

	def acept(self, btn=None):

		print("VOLVIENDO AL MENU PRINCIPAL...")
		self.notification.destroy()

# Main
if __name__ == '__main__':
	MenuInicial()
	Gtk.main()