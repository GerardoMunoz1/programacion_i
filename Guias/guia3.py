import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from datetime import date

"""
def comprobarRUT(Run):

	try:
		if len(Run <= 8):

			value = 11 - sum([ int(a)*int(b)  for a,b in zip(str(Run).zfill(8), '32765432')])%11

			return 1

		else:
			j = cloud()

	except:
		j = cloud()
"""
class VentanaPrincipal():

	def __init__(self):
		print("Constructor!")
		self.builder = Gtk.Builder()

		self.builder.add_from_file("guia3.glade")
		self.ventana = self.builder.get_object("window1")
		self.ventana.connect("destroy", Gtk.main_quit)
		self.ventana.set_default_size(500, 300)
		self.ventana.set_title("Formulario")

		self.ventana.show_all()

		self.entradaNombre = self.builder.get_object("entry1")
		self.entradaDireccion = self.builder.get_object("entry2")
		self.entradaRUN = self.builder.get_object("entry3")
		self.entradaEmail = self.builder.get_object("entry4")

		self.Close = self.builder.get_object("cerrar")
		self.Close.connect("clicked", self.Cerrar)

		self.Acept = self.builder.get_object("aceptar")
		self.Acept.connect("clicked", self.Aceptar)

		self.Limpiar = self.builder.get_object("limpia")
		self.Limpiar.connect("clicked", self.LimpiarCampos)

		self.Calendario = self.builder.get_object("calendar1")

	def Cerrar(self, btn=None):

		print("\n\nCerrando ventana principal...")
		self.ventana.destroy()

	def Aceptar(self, btn=None):

		self.fecha = self.Calendario.get_date()

		self.Nombre = self.entradaNombre.get_text()
		self.Direccion = self.entradaDireccion.get_text()
		self.Run = self.entradaRUN.get_text()
		self.Correo = self.entradaEmail.get_text()

		print("\nAbriendo formulario final...")

		print("\n\n")
		print("Nombre ingresado: ", self.Nombre)
		print("Direccion ingresada: ", self.Direccion)
		print("Fecha ingresada: ", self.fecha)
		print("Run ingresado: ", self.Run)
		print("Correo ingresado: ", self.Correo)

		today = date.today().isoformat()

		año_pc = int(today[0]+today[1]+today[2]+today[3])
		mes_pc = int(today[5]+today[6])
		dia_pc = int(today[8]+today[9])
		print("\nAÑO:", año_pc)
		print("MES:", mes_pc)
		print("DIA:", dia_pc)

		añoingresado = self.fecha[0]
		mesingresado = self.fecha[1] + 1
		diaingresado = self.fecha[2]
		print("\n")
		print("AÑO INGRESADO:", añoingresado)
		print("MES INGRESADO:", mesingresado)
		print("DIA INGRESADO:", diaingresado)

		resta_de_años = año_pc - añoingresado
		resta_de_meses = mes_pc - mesingresado
		resta_de_dias = dia_pc - diaingresado

		print("\n\nExiste una diferencia de:", resta_de_dias, "dias,", resta_de_meses, "meses y", resta_de_años, "años.")

		# comprobarRUT(self.Run)

		aux = ventanaFormulario(self.Nombre, self.Direccion, self.fecha, self.Run, self.Correo, diaingresado, mesingresado, añoingresado, resta_de_años)

	def LimpiarCampos(self, btn=None):

		print("\nLimpiando todo...")
		self.entradaNombre.set_text('')
		self.entradaDireccion.set_text('')
		self.entradaRUN.set_text('')
		self.entradaEmail.set_text('')

class ventanaFormulario():

	def __init__(self, Nombre, Direccion, fecha, Run, Correo, diaingresado, mesingresado, añoingresado, resta_de_años):

		self.builder = Gtk.Builder()
		self.builder.add_from_file("guia3.glade")
		self.ventana2 = self.builder.get_object("window2")
		self.ventana2.set_default_size(500, 300)
		self.ventana2.set_title("Formulario Completado")

		self.cerrarForum = self.builder.get_object("button1")
		self.cerrarForum.connect("clicked", self.CERRAR)

		print("\nESTAMOS IMPRIMIENDO EN FORMULARIO COMPLETADO")
		print("\nNombre ingresado: ", Nombre)
		print("Direccion ingresada: ", Direccion)
		print("Fecha ingresada: ", fecha)
		print("Run ingresado: ", Run)
		print("Correo ingresado: ", Correo)

		self.labelModificarNombre = self.builder.get_object("labelNombre")
		self.labelModificarDireccion = self.builder.get_object("labelDireccion")
		self.labelModificarFechaDia = self.builder.get_object("dia")
		self.labelModificarFechaMes = self.builder.get_object("mes")
		self.labelModificarFechaAño = self.builder.get_object("año")
		self.difDias = self.builder.get_object("labelDiferenciaDias")
		self.labelModificarRUN = self.builder.get_object("labelRUN")
		self.labelModificarCorreo = self.builder.get_object("labelCorreo")

		self.labelModificarNombre.set_text(Nombre)
		self.labelModificarDireccion.set_text(Direccion)
		self.labelModificarFechaDia.set_text(str(diaingresado))
		self.labelModificarFechaMes.set_text(str(mesingresado))
		self.labelModificarFechaAño.set_text(str(añoingresado))
		self.difDias.set_text(str(resta_de_años))
		self.labelModificarRUN.set_text(Run)
		self.labelModificarCorreo.set_text(Correo)

		self.ventana2.show_all()

	def CERRAR(self, btn=None):
		print("\n\nCerrando formulario completado...")
		self.ventana2.destroy()

class cloud():

	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("guia3.glade")
		self.dialogo = self.builder.get_object("cloudd")
		self.dialogo.set_default_size(350, 100)
		self.dialogo.set_title("UPS!")

		self.cerrarDialogo = self.builder.get_object("cerrarAlerta")
		self.cerrarDialogo.connect("clicked", self.cirrar)

		self.dialogo.show_all()

	def cirrar(self, btn=None):
		print("\nCerrando alerta...")
		self.dialogo.destroy()

if __name__ == '__main__':
	VentanaPrincipal()
	Gtk.main()
