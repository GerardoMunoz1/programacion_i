# Examen Programación I

Profesor: Fabio Durán.
Estudiante: Gerardo Muñoz.
Carrera: Ingeniería Civil en Bioinformática.

# Problema propuesto
Se pide construir un simulador de teléfono con su respectiva agenda
que pueda guardar contactos con sus datos respectivos.

# Faltas del programa
-No logra hacer convalidación de la agenda con el número que se está
ingresando.
-No logra almacenar el número ingresado.
-No logra borrar paso a paso con el botón de "Deshacer", solamente borra 1 vez
y no "x" veces apretado el botón.

# Novedades del programa
-Todos los botones del teclado numérico del teléfono son funcionales.
-Funciona tanto, el teclado numérico como también la entrada de texto para escribir
mediante teclado.
-Si no hay un número escrito, no permitirá la opción de llamar y agregar como contacto,
debido a que no existe tal número.
-Guarda los contactos ingresados en formato (.json), lo que permite un mayor orden
gracias a sus identaciones.

# Referencias de la PEP-8
-Solo se usan espacios y no tabulaciones.
-Los comentarios, están escritos sobre cada clase, dando así una mayor claridad del cógido.

Dentro del código, todo lo que está comentado fue un intento de algo, que por alguna razón, no pudo
ser concretada.
