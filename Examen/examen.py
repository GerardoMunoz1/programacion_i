import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json
import time

def num(boton, entrada, label):
    a = boton.get_label()
    texto = entrada.get_text() + a
    if len(texto) <= 8:
        entrada.set_text(texto)
    elif len(texto) >= 9:
        label.set_text(" No ingrese más números \n")

# Método para abrir archivos json
def open_file():
	try:
		with open('contactos.json', 'r') as file:
			data = json.load(file)
		file.close()
	except IOError:
		data = []

	return data

# Método para guardar archivos json
def save_file(data):
    print("Contacto guardado exitosamente.")

    # Guardamos todos los datos enviados en 'data' en formato Json
    with open('contactos.json', 'w') as file:
        json.dump(data, file, indent=4)
    file.close()
"""
def check_entradas_guardado(nombre, apodo, email, trabajo):

	flag = 0

	if ((nombre == '') and (apodo == '') and (email == '') and (trabajo == '')):
		print("\n[ERROR]")
		flag = 1

	else:
		flag = 0

		print("BANDERA:", flag)

	if (flag == 0):
			
		
		print("abriendo compare")

	elif (flag != 0):

		t = error()
"""
# Main_core
class Main():

	def __init__(self):
		print("Constructor!")
		self.builder = Gtk.Builder()
		self.builder.add_from_file("examen.glade")
		self.Main = self.builder.get_object("mainWindow")
		self.Main.connect("destroy", Gtk.main_quit)
		self.Main.set_default_size(400, 600)
		self.Main.set_title("Teléfono")

		self.btnLlamar = self.builder.get_object("buttonCall")
		self.btnLlamar.connect("clicked", self.Llamar)

		self.btnCancelar = self.builder.get_object("buttonCancel")
		self.btnCancelar.connect("clicked", self.Cancelar)

		self.btnGuardar = self.builder.get_object("buttonSave")
		self.btnGuardar.connect("clicked", self.Guardar)

		self.btnClear = self.builder.get_object("buttonClear")
		self.btnClear.connect("clicked", self.Limpiar)

		self.btnUndo = self.builder.get_object("buttonUndo")
		self.btnUndo.connect("clicked", self.borrar)

		self.numero_ingresado = self.builder.get_object("entry1")

		self.numero_entry = ('+569 ')
	
		self.numero_1 = self.builder.get_object("b1")
		self.numero_1.connect("clicked", self.num1)

		self.numero_2 = self.builder.get_object("b2")
		self.numero_2.connect("clicked", self.num2)

		self.numero_3 = self.builder.get_object("b3")
		self.numero_3.connect("clicked", self.num3)

		self.numero_4 = self.builder.get_object("b4")
		self.numero_4.connect("clicked", self.num4)

		self.numero_5 = self.builder.get_object("b5")
		self.numero_5.connect("clicked", self.num5)

		self.numero_6 = self.builder.get_object("b6")
		self.numero_6.connect("clicked", self.num6)

		self.numero_7 = self.builder.get_object("b7")
		self.numero_7.connect("clicked", self.num7)

		self.numero_8 = self.builder.get_object("b8")
		self.numero_8.connect("clicked", self.num8)

		self.numero_9 = self.builder.get_object("b9")
		self.numero_9.connect("clicked", self.num9)

		self.numero_0 = self.builder.get_object("b0")
		self.numero_0.connect("clicked", self.num0)

		self.Main.show_all()

	def Llamar(self, btn=None):

		self.numero = self.numero_ingresado.get_text()
		print("Llamando...\n")
		print(self.numero)

		flag = 0

		if (self.numero == ''):
			print("\n[ERROR]")
			flag = 1

		else:
			flag = 0

		print("BANDERA:", flag)

		if (flag == 0):
			aux = dialogoLlamando(self.numero)

		elif (flag != 0):

			t = error()

	def Cancelar(self, btn=None):
		print("Cancelando todo...")
		self.Main.destroy()

	def Guardar(self, btn=None):
		print("\nGuardando...")

		self.numero = self.numero_ingresado.get_text()

		flag = 0

		if (self.numero == ''):
			print("\n[ERROR]")
			flag = 1

		else:
			flag = 0

		print("BANDERA:", flag)

		if (flag == 0):
			
			aux = guardarContacto(self.numero)

		elif (flag != 0):

			t = error()

	def num1(self, numero_entry):
		print("\nPresionaste el número 1.")
		self.numero_entry = self.numero_entry + '1'
		print("Numero:", self.numero_entry)
		self.numero_ingresado.set_text(self.numero_entry)

	def num2(self, numero_entry):
		print("\nPresionaste el número 2.")
		self.numero_entry = self.numero_entry + '2'
		print("Numero:", self.numero_entry)
		self.numero_ingresado.set_text(self.numero_entry)

	def num3(self, numero_entry):
		print("\nPresionaste el número 3.")
		self.numero_entry = self.numero_entry + '3'
		print("Numero:", self.numero_entry)
		self.numero_ingresado.set_text(self.numero_entry)

	def num4(self, numero_entry):
		print("\nPresionaste el número 4.")
		self.numero_entry = self.numero_entry + '4'
		print("Numero:", self.numero_entry)
		self.numero_ingresado.set_text(self.numero_entry)

	def num5(self, numero_entry):
		print("\nPresionaste el número 5.")
		self.numero_entry = self.numero_entry + '5'
		print("Numero:", self.numero_entry)
		self.numero_ingresado.set_text(self.numero_entry)

	def num6(self, numero_entry):
		print("\nPresionaste el número 6.")
		self.numero_entry = self.numero_entry + '6'
		print("Numero:", self.numero_entry)
		self.numero_ingresado.set_text(self.numero_entry)

	def num7(self, numero_entry):
		print("\nPresionaste el número 7.")
		self.numero_entry = self.numero_entry + '7'
		print("Numero:", self.numero_entry)
		self.numero_ingresado.set_text(self.numero_entry)

	def num8(self, numero_entry):
		print("\nPresionaste el número 8.")
		self.numero_entry = self.numero_entry + '8'
		print("Numero:", self.numero_entry)
		self.numero_ingresado.set_text(self.numero_entry)

	def num9(self, numero_entry):
		print("\nPresionaste el número 9.")
		self.numero_entry = self.numero_entry + '9'
		print("Numero:", self.numero_entry)
		self.numero_ingresado.set_text(self.numero_entry)

	def num0(self, numero_entry):
		print("\nPresionaste el número 0.")
		self.numero_entry = self.numero_entry + '0'
		print("Numero:", self.numero_entry)
		self.numero_ingresado.set_text(self.numero_entry)

	def borrar(self, btn=None):
		print("\nDeshaciendo...")
		s = len(self.numero_entry)

		flag = 0

		if (flag == 0):

			for event in ['activate']:
				cadena = self.numero_entry
				temp = len(cadena)

				cadena = cadena[:temp - 1]

			print(str(cadena))
			flag = 1
			print("BANDERA:", flag)

		else:
			print("no entramo")
			flag = 0

		self.numero_ingresado.set_text(cadena)

		return cadena
		return flag

	def Limpiar(self, btn=None):
		print("\nLimpiando entrada...")
		self.numero_ingresado.set_text('')
		self.numero_entry = ('+569 ')
# Clase para guardar los contactos
class guardarContacto():

	def __init__(self, numero):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("examen.glade")
		self.forum = self.builder.get_object("saveWindow")
		self.forum.set_default_size(600, 200)
		self.forum.set_title("Guardar contacto")

		self.buttonCancelSave = self.builder.get_object("btnCancelSave")
		self.buttonCancelSave.connect("clicked", self.cancel)

		self.buttonSave = self.builder.get_object("btnSave")
		self.buttonSave.connect("clicked", self.guardar_numero)

		self.numero_ingresado = self.builder.get_object("entry1")

		numero = self.numero_ingresado.get_text()

		self.entradaNombre = self.builder.get_object("entryName")
		self.entradaApodo = self.builder.get_object("entryApodo")
		self.entradaEmail = self.builder.get_object("entryEmail")
		self.entradaTrabajo = self.builder.get_object("entryJob")
		
		print("NUMERO A GUARDAR:", numero)

		self.forum.show_all()

	def cancel(self, btn=None):
		print("\nCancelando guardar contacto...")
		self.forum.destroy()

	def guardar_numero(self, btn=None):
		numero = self.numero_ingresado.get_text()

		self.Nombre = self.entradaNombre.get_text()
		self.Apodo = self.entradaApodo.get_text()
		self.Email = self.entradaEmail.get_text()
		self.Trabajo = self.entradaTrabajo.get_text()

		print("NUMERO INGRESADO:", numero)
		print("NOMBRE INGRESADO:", self.Nombre)
		print("APODO INGRESADO:", self.Apodo)
		print("EMAIL INGRESADO:", self.Email)
		print("TRABAJO INGRESADO:", self.Trabajo)
		print("\n")

		#check_entradas_guardado(self.Nombre, self.Apodo, self.Email, self.Trabajo)

		self.forum.destroy()

		x = {}
		
		x = {"Numero": [{"Nombre": self.Nombre, "Apodo": self.Apodo, "Email": self.Email, "Trabajo": self.Trabajo}]}

		f = open_file()
		f.append(x)
		save_file(f)
# Clase para mostrar el dialogo de llamado
class dialogoLlamando():

	def __init__(self, numero):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("examen.glade")
		self.ventana = self.builder.get_object("callWindow")
		self.ventana.set_default_size(300, 200)
		self.ventana.set_title("Llamando")

		self.colgar = self.builder.get_object("btnEnd")
		self.colgar.connect("clicked", self.finLlamada)

		self.impresion_num = self.builder.get_object("labelNumero")
		self.impresion_num.set_text(numero)

		self.ventana.show_all()

	def finLlamada(self, btn=None):

		print("\nLlamada finalizada...")
		self.ventana.destroy()

		aux = notificacion()
# Clase para mostrar una notificación
class notificacion():

	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("examen.glade")
		self.atencion = self.builder.get_object("dialog1")
		self.atencion.set_default_size(200, 200)
		self.atencion.set_title("Finalizada")

		self.cerrar = self.builder.get_object("btnClose")
		self.cerrar.connect("clicked", self.close)

		self.atencion.show_all()

	def close(self, btn=None):
		self.atencion.destroy()
# Clase para mostrar un error
class error():

	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("examen.glade")
		self.ups = self.builder.get_object("dialog2")
		self.ups.set_default_size(200, 200)
		self.ups.set_title("UPS")

		self.btnOk = self.builder.get_object("btn_Ok")
		self.btnOk.connect("clicked", self.adios)

		self.ups.show_all()

	def adios(self, btn=None):
		self.ups.destroy()
# Main
if __name__ == '__main__':
	R = Main()
	Gtk.main()